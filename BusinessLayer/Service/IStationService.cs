﻿using BusinessLayer.Entity;

namespace BusinessLayer.Service
{
    /// <summary>
    /// Defines the Station service operations contract.
    /// </summary>
    public interface IStationService
    {
        /// <summary>
        /// Searches the station started with the name parameter value.
        /// </summary>
        /// <param name="name">The station name filter.</param>
        /// <returns> The <see cref="StationSearchResult"/> containing the collection of stations and the next available chars. </returns>
        StationSearchResult SearchStartingWith(string name);
    }
}
