﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Collection
{
    /// <summary>
    /// This class contains the implementation of Trie or Prefix Tree data structure.
    /// </summary>
    public class Trie
    {
        /// <summary>
        /// The <see cref="StringBuilder"/> instance used for string manipulation.
        /// </summary>
        static readonly StringBuilder StringBuilder = new StringBuilder();


        /// <summary>
        /// This class represents the Trie node.
        /// </summary>
        class TrieNode
        {
            /// <summary>
            /// This field contains the trie node childs and is used as a lookup table.
            /// </summary>
            private readonly Dictionary<char, TrieNode> trieNodes;

            /// <summary>
            /// Initializes a new instance of the <see cref="TrieNode"/> class.
            /// </summary>
            public TrieNode()
            {
                trieNodes = new Dictionary<char, TrieNode>();
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="TrieNode"/> class.
            /// </summary>
            /// <param name="c">The c.</param>
            public TrieNode(char c) : this()
            {
                Char = c;
            }

            /// <summary>
            /// Gets a value indicating whether this instance is a leaf trie node.
            /// </summary>
            /// <value>
            ///   <c>true</c> if this instance is leaf; otherwise, <c>false</c>.
            /// </value>
            private bool IsLeaf { get { return trieNodes.Count == 0; } }

            /// <summary>
            /// Gets or sets the trie node character.
            /// </summary>
            /// <value>
            /// The char.
            /// </value>
            private char Char { get; set; }

            /// <summary>
            /// Gets the child node with the indicated character.
            /// </summary>
            /// <param name="c">The character.</param>
            /// <returns></returns>
            public TrieNode GetChild(char c)
            {
                TrieNode node;
                trieNodes.TryGetValue(c, out node);
                return node;
            }

            /// <summary>
            /// Adds the child node into the current TrieNode lookup table.
            /// </summary>
            /// <param name="node">The node.</param>
            public void AddChild(TrieNode node)
            {
                trieNodes.Add(node.Char, node);
            }

            /// <summary>
            /// Gets the childs terms from the current trie node.
            /// </summary>
            /// <param name="term">The starting term.</param>
            /// <returns></returns>
            public IEnumerable<string> GetChildsTerms(string term)
            {
                if (IsLeaf)
                    return new List<string> { term };

                var terms = new List<string>();

                foreach (var pair in trieNodes)
                {
                    StringBuilder.Clear();
                    StringBuilder.Append(term);

                    terms.AddRange(pair.Value.GetChildTermsInternal(StringBuilder));
                }

                return terms;
            }

            private IEnumerable<string> GetChildTermsInternal(StringBuilder sb)
            {
                // Do not append the control char
                if (new char() != Char)
                    sb.Append(Char);
                
                if (IsLeaf)
                    return new[] { sb.ToString() };

                var terms = new List<string>();
                foreach (var trieNode in trieNodes)
                {
                    terms.AddRange(trieNode.Value.GetChildTermsInternal(new StringBuilder(sb.ToString())));

                }

                return terms;
            }
        }

        /// <summary>
        /// The trie head node.
        /// </summary>
        private readonly TrieNode head;

        /// <summary>
        /// Initializes a new instance of the <see cref="Trie"/> class.
        /// </summary>
        public Trie()
        {
            head = new TrieNode();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Trie"/> class populated with terms.
        /// </summary>
        /// <param name="terms">The terms.</param>
        public Trie(IEnumerable<string> terms)
            : this()
        {
            foreach (var term in terms)
            {
                Add(term);
            }
        }

        /// <summary>
        /// Finds the specified prefix.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns></returns>
        public IEnumerable<string> Find(string prefix)
        {
            if (prefix == null)
                throw new ArgumentNullException("prefix");

            StringBuilder.Clear();

            TrieNode node = head;
            foreach (char prefixChar in prefix)
            {
                var child = node.GetChild(prefixChar);
                node = child;

                if (child == null)
                    return new string[0];

                StringBuilder.Append(prefixChar);
            }

            return node.GetChildsTerms(StringBuilder.ToString());
        }

        /// <summary>
        /// Gets all terms existents in the Trie.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetAllTerms()
        {
            return head.GetChildsTerms(string.Empty);
        }

        /// <summary>
        /// Adds the specified term into the Trie.
        /// </summary>
        /// <param name="term">The term.</param>
        public void Add(string term)
        {
            if (term == null)
                throw new ArgumentNullException("term");


            TrieNode node = head;

            // Adding control char
            var prefixChars = new List<char>(term){ new char() };
            foreach (char prefixChar in prefixChars)
            {
                var child = node.GetChild(prefixChar);

                if (child == null)
                {
                    child = new TrieNode(prefixChar);
                    node.AddChild(child);
                }

                node = child;
            }
        }

        /// <summary>
        /// Adds the specified terms into the trie.
        /// </summary>
        /// <param name="terms">The terms.</param>
        public void Add(IEnumerable<string> terms)
        {
            foreach (var term in terms)
            {
                Add(term);
            }
        }
    }
}
