﻿using System.Collections.Generic;
using BusinessLayer.Collection;

namespace BusinessLayer.Repository
{
    /// <summary>
    /// The Station repository implemented with the Trie data structure.
    /// </summary>
    public class TrieStationRepository : IStationRepository
    {
        /// <summary>
        /// The trie data structure.
        /// </summary>
        private readonly Trie trie;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrieStationRepository"/> class.
        /// </summary>
        /// <param name="stations">The stations.</param>
        public TrieStationRepository(IEnumerable<string> stations)
        {
            trie = new Trie(stations);
        }

        /// <summary>
        /// Gets all stations started with the name parameter value.
        /// </summary>
        /// <param name="name">The station name filter.</param>
        /// <returns>
        /// The list of stations.
        /// </returns>
        public IEnumerable<string> GetAllStartedWithName(string name)
        {
            return trie.Find(name);
        }
    }
}
