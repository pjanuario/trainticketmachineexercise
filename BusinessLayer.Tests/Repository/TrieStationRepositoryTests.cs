﻿using System.Collections.Generic;
using BusinessLayer.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace BusinessLayer.Tests.Repository
{
    [TestClass]
    public class TrieStationRepositoryTests
    {
        [TestMethod]
        public void TestGetAllStartedWithPartialStationName()
        {
            // Arrange
            var datasource = new List<string> { "DARTFORD", "DARTMOUTH", "TOWER HILL", "DERBY" };
            var expected = new List<string> { "DARTFORD", "DARTMOUTH" };

            // Act
            var actual = new TrieStationRepository(datasource).GetAllStartedWithName("DART");

            // Assert
            Assert.IsTrue(expected.SequenceEqual(actual));
        }

        [TestMethod]
        public void TestGetAllStartedWithFullStationName()
        {
            // Arrange
            var datasource = new List<string> { "DARTFORD", "LIVERPOOL", "PADDINGTON" };
            var expected = new List<string>{ "LIVERPOOL" };

            // Act
            var actual = new TrieStationRepository(datasource).GetAllStartedWithName("LIVERPOOL");

            // Assert
            Assert.IsTrue(expected.SequenceEqual(actual));
        }

        [TestMethod]
        public void TestGetAllStartedWithPartialStationNameAndSpaceAfter()
        {
            // Arrange
            var datasource = new List<string> { "LIVERPOOL", "LIVERPOOL LIME STREET", "PADDINGTON" };
            var expected = new List<string> { "LIVERPOOL", "LIVERPOOL LIME STREET" };

            // Act
            var actual = new TrieStationRepository(datasource).GetAllStartedWithName("LIVERPOOL");

            // Assert
            Assert.IsTrue(expected.SequenceEqual(actual));
        }

        [TestMethod]
        public void TestGetAllStartedWithUnexistentStationName()
        {
            // Arrange
            var datasource = new List<string> { "EUSTON", "LONDON BRIDGE", "VICTORIA" };
            var expected = new List<string>(0);

            // Act
            var actual = new TrieStationRepository(datasource).GetAllStartedWithName("KINGS CROSS");

            // Assert
            Assert.IsTrue(expected.SequenceEqual(actual));
        }
    }
}
