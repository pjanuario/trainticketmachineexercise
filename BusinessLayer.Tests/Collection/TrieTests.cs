﻿using System.Linq;
using BusinessLayer.Collection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessLayer.Tests.Collection
{
    [TestClass]
    public class TrieTests
    {
        [TestMethod]
        public void TestTrieAddWithSingleCharStringTerm()
        {
            // Arrange
            var terms = new[] { "P" };
            var trie = new Trie();
            var expected = terms;

            // Act
            trie.Add(terms);
            var actual = trie.GetAllTerms();

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Count(), expected.Where(actual.Contains).Count());
        }

        [TestMethod]
        public void TestTrieAddWithSingleTerm()
        {
            // Arrange
            var terms = new[] { "PE" };
            var trie = new Trie();
            var expected = terms;

            // Act
            trie.Add(terms);
            var actual = trie.GetAllTerms();

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Count(), expected.Where(actual.Contains).Count());
        }

        [TestMethod]
        public void TestTrieAddWithMultipleTermsStartedWithSamePrefix()
        {
            // Arrange
            var terms = new[] { "PET", "PED" };
            var trie = new Trie();
            var expected = terms;

            // Act
            trie.Add(terms);
            var actual = trie.GetAllTerms();

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Count(), expected.Where(actual.Contains).Count());

        }

        [TestMethod]
        public void TestGetTermsWithSingleChar()
        {
            // Arrange
            var terms = new[] { "P" };
            var trie = new Trie(terms);

            var expected = terms;

            // Act
            var actual = trie.Find("P");

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Count(), expected.Where(actual.Contains).Count());
        }

        [TestMethod]
        public void TestGetTermsWithSingleCharAndMultipleBranches()
        {
            // Arrange
            var terms = new[] { "PETER", "PATI" };
            var trie = new Trie(terms);

            var expected = terms;

            // Act
            var actual = trie.Find("P");

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Count(), expected.Where(actual.Contains).Count());
        }

        [TestMethod]
        public void TestGetTermsWithFullTerm()
        {
            // Arrange
            var terms = new[] { "PE" };
            var trie = new Trie(terms);

            var expected = terms;

            // Act
            var actual = trie.Find("PE");

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Count(), expected.Where(actual.Contains).Count());
        }

        [TestMethod]
        public void TestGetTermsWithFullTermAndBranch()
        {
            // Arrange
            var terms = new[] { "PEDRO", "PEDROS" };
            var trie = new Trie(terms);

            var expected = terms;

            // Act
            var actual = trie.Find("PEDRO");

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Count(), expected.Where(actual.Contains).Count());
        }

        [TestMethod]
        public void TestGetTermsWithPartialTermAndSingleBranche()
        {
            // Arrange
            var terms = new[] { "PET" };
            var trie = new Trie(terms);

            var expected = terms;

            // Act
            var actual = trie.Find("PE");

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Count(), expected.Where(actual.Contains).Count());
        }

        [TestMethod]
        public void TestGetTermsWithPartialTermAndMultipleBranches()
        {
            // Arrange
            var terms = new[] { "PET", "PED" };
            var trie = new Trie(terms);

            var expected = terms;

            // Act
            var actual = trie.Find("PE");

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Count(), expected.Where(actual.Contains).Count());
        }
    }
}
